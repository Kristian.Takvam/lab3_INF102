package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
    	/*if (list.isEmpty()) {
    		return 0;
    	}
    	else {
    		Long number = list.get(0);
    		return number + sum(list.subList(1, list.size()));
    		
    	}*/
    	return helpSum(list, 0);
    }
    /*
     * Prøvde først å bruke subList(), men det var fõr treigt tydeligvis.
     * 
     * Her er en hjelpefunksjon som gjør at jeg kan ha med en index som parameter.
     * Denne indeksen brukes fôr å holde styr på posisjonen i listen. 
     * Dette blir mer effektiv enn å bruke sublist.
     */
    private long helpSum(List<Long> list, int index) {
    	if (index == list.size()) {
    		return 0;
    	}
    	return list.get(index) + helpSum(list, index+1);
    }
}