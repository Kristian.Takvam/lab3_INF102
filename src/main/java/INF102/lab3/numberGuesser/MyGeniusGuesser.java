package INF102.lab3.numberGuesser;

import java.util.Random;

public class MyGeniusGuesser implements IGuesser {

	private Random rand = new Random();
	private int lowerbound;
	private int upperbound;

	@Override
    public int findNumber(RandomNumber number) {
		lowerbound = number.getLowerbound();
		upperbound = number.getUpperbound();
		
		int numberGuess;
		for (int i = 0; i<upperbound; i++) {
			numberGuess = i;
			if (number.guess(numberGuess) == 0) {
				return numberGuess;
			}
		} 
		return 0;
    }

}
