package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {

    	// Edge cases
        int n = numbers.size();
        if (n == 1)
            return numbers.get(0);
        
        if (numbers.get(0) > numbers.get(1))
            return numbers.get(0);

        if (numbers.get(n-1) > numbers.get(n-2))
            return numbers.get(n-1);
    	
    	
        int prev = numbers.get(0);
        int current = numbers.get(1);
        int next = numbers.get(2);
    	
        //return 0;
        if (isPeak(prev, current, next)) {
            return current;
        }
        
    	return peakElement(numbers.subList(1, numbers.size()));
    }
    
    private boolean isPeak(int prev, int current, int next) {
        return current > prev && current > next;
    }

}
